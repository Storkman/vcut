#define LEN(X) (sizeof(X)/sizeof((X)[0]))

void	die(const char *fmt, ...);
void*	emalloc(size_t);
int	error(const char *fmt, ...);
