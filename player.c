#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <mpv/client.h>
#include <SDL2/SDL.h>

#include "common.h"
#include "player.h"

struct prop_handler {
	const char	*name;
	void	(*fn)(mpv_event_property*);
	mpv_format	format;
};

static void	prop_audio_time(mpv_event_property*);
static void	prop_video_time(mpv_event_property*);
static int	register_prop_handlers(void);
static void	run_prop_handler(mpv_event*);

mpv_handle	*mpv;
double	player_audio_time = 0.0;
double	player_video_time = 0.0;

static const struct prop_handler prop_handlers[] = {
	{NULL,	NULL,	MPV_FORMAT_NONE },
	{"audio-pts",	prop_audio_time,	MPV_FORMAT_DOUBLE },
	{"time-pos",	prop_video_time,	MPV_FORMAT_DOUBLE },
};

int
player_abloop(double a, double b, int seekto)
{
	int err;
	if ((err = player_setprop("ab-loop-a", "%g", a)))
		return error("failed to set up A-B loop: %s\n", mpv_error_string(err));
	if ((err = player_setprop("ab-loop-b", "%g", b)))
		return error("failed to set up A-B loop: %s\n", mpv_error_string(err));
	if (seekto == 1)
		return player_seek(b);
	else
		return player_seek(a);
}

int
player_check_playhead(void)
{
	int err;
	if ((err = mpv_get_property(mpv, "time-pos", MPV_FORMAT_DOUBLE, &player_video_time))) {
		if (err == MPV_ERROR_PROPERTY_UNAVAILABLE)
			return 0;
		else
			return error("failed to get current position: %s\n", mpv_error_string(err));
	}
	return 0;
}

#define MAXARGS 64

int
player_cmd(const char *cmd, ...)
{
	int i=0, err;
	va_list fa;
	const char *arg, **argb;
	const char *args[MAXARGS+1];
	
	va_start(fa, cmd);
	
	args[0] = cmd;
	argb = args+1;
	/* Loop over the arguments. NULL is necessary as a sentinel value for both varargs and mpv. */
	do {
		if (i > MAXARGS)
			return MPV_ERROR_COMMAND;
		*argb = arg = va_arg(fa, const char*);
		argb++;
		i++;
	} while (arg);
	va_end(fa);
	
	if ((err = mpv_command(mpv, args)))
		return error("command failed: %s: %s\n", cmd, mpv_error_string(err));
	return 0;
}

void
player_create(void)
{
	int err;
	mpv = mpv_create();
	if (!mpv)
		die("Failed to initialize mpv\n");
	
	player_opt("osc", "yes");
	player_opt("force-window", "yes");
	player_opt("config", "yes");
	player_opt("input-default-bindings", "yes");
	player_opt("input-vo-keyboard", "yes");
	player_opt("ytdl", "yes");
	
	player_opt("pause", "yes");
	player_opt("keep-open", "always");
	
	err = register_prop_handlers();
	if (err)
		die("failed to register mpv property handlers: %s\n", mpv_error_string(err));
}

const char*
player_keysym(SDL_Keycode ks)
{
	const char *name;
	size_t len;
	switch (ks) {
	case SDLK_BACKSPACE: return "BS";
	case SDLK_TAB: return "TAB";
	case SDLK_RETURN: return "ENTER";
	case SDLK_ESCAPE: return "ESC";
	case SDLK_DELETE: return "DEL";
	case SDLK_RIGHT: return "RIGHT";
	case SDLK_LEFT: return "LEFT";
	case SDLK_UP: return "UP";
	case SDLK_DOWN: return "DOWN";
	case SDLK_SPACE: return "SPACE";
	}
	
	name = SDL_GetKeyName(ks);
	len = strlen(name);
	if (len == 0)
		return NULL;
	if (len == 1)
		return name;
	if (name[0] != 'F' || len > 3)
		return NULL;
	if (name[1] < '0' || name[1] > '9')
		return NULL;
	if (len == 3 && (name[2] < '0' || name[2] > '9'))
		return NULL;
	return name;
}

mpv_event*
player_next_event(void)
{
	mpv_event *ev = mpv_wait_event(mpv, 0);
	run_prop_handler(ev);
	return ev;
}

int
player_noabloop(void)
{
	int err;
	if ((err = player_setprop("ab-loop-a", "no")))
		return error("failed to clear A-B loop: %s\n", mpv_error_string(err));
	if ((err = player_setprop("ab-loop-b", "no")))
		return error("failed to clear A-B loop: %s\n", mpv_error_string(err));
	return 0;
}

void
player_opt(const char *opt, const char *fmt, ...)
{
	int err;
	char buf[2048];
	va_list arg;

	va_start(arg, fmt);
	buf[sizeof(buf)-1] = 0xFF;
	vsnprintf(buf, sizeof(buf), fmt, arg);
	if (buf[sizeof(buf)-1] == 0x00)
		die("Oversized option: %s\n", opt);
	va_end(arg);
	
	if ((err = mpv_set_property_string(mpv, opt, buf)))
		die("Failed to initialize mpv: %s=%s: %s\n", opt, buf, mpv_error_string(err));
}

int
player_paused(void)
{
	int p, err;
	err = mpv_get_property(mpv, "pause", MPV_FORMAT_FLAG, &p);
	if (err) {
		error("failed to retrieve pause state: %s\n", mpv_error_string(err));
		return 0;
	}
	return p;
}

int
player_seek(double where)
{
	char b[1024];
	int n;
	n = snprintf(b, sizeof(b), "%g", where);
	if (n >= sizeof(b))
		return -1;
	return player_cmd("seek", b, "absolute", NULL);
}

int
player_setprop(const char *opt, const char *fmt, ...)
{
	int err;
	char buf[4096];
	va_list arg;
	
	va_start(arg, fmt);
	buf[sizeof(buf)-1] = 0xFF;
	vsnprintf(buf, sizeof(buf), fmt, arg);
	if (buf[sizeof(buf)-1] == 0x00)
		return error("Oversized property: %s\n", opt);
	va_end(arg);
	
	if ((err = mpv_set_property_string(mpv, opt, buf)))
		return error("Failed to set property: %s=%s: %s\n", opt, buf, mpv_error_string(err));
	return 0;
}

void
prop_audio_time(mpv_event_property *e)
{
	double *d = e->data;
	player_audio_time = *d;
}

void
prop_video_time(mpv_event_property *e)
{
	double *d = e->data;
	player_video_time = *d;
}

#define TAG_BITS	6
#define TAG_SHIFT	(64 - TAG_BITS)
#define TAG_MASK	( (((uint64_t) 1) << (TAG_SHIFT+1)) - 1 )

int
register_prop_handlers(void)
{
	const struct prop_handler *p;
	int err;
	uint64_t i;
	for (i = 0; i < LEN(prop_handlers); i++) {
		p = prop_handlers + i;
		if (!p->format)
			continue;
		err = mpv_observe_property(mpv, i << TAG_SHIFT, p->name, p->format);
		if (err)
			return err;
	}
	return 0;
}

void
run_prop_handler(mpv_event *ev)
{
	mpv_event_property *p;
	const struct prop_handler *ph;
	int tag;
	if (ev->event_id != MPV_EVENT_PROPERTY_CHANGE
	||  ev->reply_userdata & ~TAG_MASK)
		return;
	tag = ev->reply_userdata >> TAG_SHIFT;
	p = ev->data;
	if (tag >= LEN(prop_handlers))
		return;
	ph = &prop_handlers[tag];
	if (!ph->fn || ph->format != p->format || strcmp(ph->name, p->name) != 0)
		return;
	return ph->fn(p);
}
