#define _DEFAULT_SOURCE
#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <poll.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>

#include <mpv/client.h>
#include <SDL2/SDL.h>

#include "common.h"
#include "player.h"

static void	event_callback(void*);
static int	handlemsg(mpv_event_client_message*);
static int	mpvevents(void);
static void	request_quit(void);
static void	timefmt(char* buf, int len, double time);

static void	sdl_button(int x, int y, Uint32 btn, bool bdown);
static void	sdl_init(void);
static bool	sdl_events(void);
static void	sdl_key(SDL_Keycode ksym, bool kdown);
static void	sdl_motion(int x, int y, Uint32 btn);
static void	sdl_redraw(void);
static void	sdl_resize(void);

Uint32	wake_event;

SDL_Window	*window;
SDL_Renderer	*render;

static int	winw, winh;

void
die(const char *fmt, ...)
{
	va_list ap;
	
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	
	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	}
	
	abort();
}

void*
emalloc(size_t s)
{
	void *p = malloc(s);
	if (!p)
		die("out of memory\n");
	return memset(p, 0, s);
}

int
error(const char *fmt, ...)
{
	va_list ap;
	
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	
	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	}
	
	return -1;
}

void
event_callback(void *d)
{
	SDL_Event ev;
	SDL_zero(ev);
	ev.type = wake_event;
	SDL_PushEvent(&ev);
}

int
handlemsg(mpv_event_client_message *msg)
{
	return 0;
}

int
main(int argc, char* argv[])
{
	char **fp;
	int err;
	
	err = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
	if (err)
		die("failed to initialize SDL: %s\n", SDL_GetError());
	wake_event = SDL_RegisterEvents(1);
	
	player_create();
	mpv_set_wakeup_callback(mpv, event_callback, NULL);
	
	sdl_init();
	if ((err = mpv_initialize(mpv)))
		die("failed to initialize mpv: %s\n", mpv_error_string(err));
	
	fp = argv+1;
	while (*fp)
		player_cmd("loadfile", *fp++, "append-play", NULL);
	
	for (;;) {
		bool redraw;
		redraw = sdl_events();
		if (mpvevents())
			break;
		if (redraw)
			sdl_redraw();
	}
	
	return 0;
}

int
mpvevents(void)
{
	mpv_event *ev;
	for (;;) {
		ev = player_next_event();
		switch (ev->event_id) {
		case MPV_EVENT_NONE:
			return 0;
		case MPV_EVENT_SHUTDOWN:
			return 1;
		case MPV_EVENT_CLIENT_MESSAGE:
			if (handlemsg(ev->data))
				return 1;
			break;
		}
	}
	return 0;
}

void
request_quit(void)
{
	exit(0);
}

/* timefmt prints the given time into the buffer in the "0:00:00.000" format. */
void
timefmt(char *buf, int buf_cap, double time)
{
	int h, m, s, ms;
	assert(buf_cap >= 8);
	s = time;
	ms = (time - ((double) s)) * 1000;
	m = s / 60;
	h = m / 60;
	m %= 60;
	s %= 60;
	snprintf(buf, buf_cap, "%01d:%02d:%02d.%03d", h, m, s, ms);
}

void
sdl_button(int x, int y, Uint32 btn, bool bdown)
{
}

bool
sdl_events(void)
{
	SDL_Event ev;
	bool need_redraw;
	need_redraw = false;
	if (!SDL_WaitEvent(NULL))
		die("event loop error: %s\n", SDL_GetError());
	while (SDL_PollEvent(&ev)) {
		if (ev.type == wake_event)
			continue;
		switch (ev.type) {
		case SDL_KEYDOWN:
		case SDL_KEYUP:
			sdl_key(ev.key.keysym.sym, ev.key.state == SDL_PRESSED);
			break;
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
			sdl_button(ev.button.x, ev.button.y,
				ev.button.button, ev.button.state == SDL_PRESSED);
			break;
		case SDL_MOUSEMOTION:
			sdl_motion(ev.motion.x, ev.motion.y, ev.motion.state);
			break;
		case SDL_WINDOWEVENT:
			switch (ev.window.event) {
			case SDL_WINDOWEVENT_CLOSE:
				request_quit();
				break;
			case SDL_WINDOWEVENT_SIZE_CHANGED:
				winw = ev.window.data1;
				winh = ev.window.data2;
				sdl_resize();
				need_redraw = true;
			case SDL_WINDOWEVENT_EXPOSED:
				need_redraw = true;
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
	}
	return need_redraw;
}

void
sdl_init(void)
{
	int err;
	err = SDL_CreateWindowAndRenderer(winw, winh, SDL_WINDOW_RESIZABLE,
		&window, &render);
	if (err)
		die("failed to create main window: %s\n", SDL_GetError());
	SDL_SetRenderDrawColor(render, 20, 20, 20, 255);
	SDL_RenderClear(render);
}

void
sdl_key(SDL_Keycode ksym, bool kdown)
{
	const char *mk;
	mk = player_keysym(ksym);
	if (!mk)
		return;
	player_cmd(kdown ? "keydown" : "keyup", mk, NULL);
}

void
sdl_motion(int x, int y, Uint32 btn)
{
}

void
sdl_redraw(void)
{
	SDL_SetRenderDrawColor(render, 20, 150, 120, 255);
	SDL_RenderClear(render);
	SDL_RenderPresent(render);
}

void
sdl_resize(void)
{
}
