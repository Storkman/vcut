extern mpv_handle	*mpv;
extern double	player_audio_time;
extern double	player_video_time;

/* player_abloop sets up an A-B loop and seeks to A (if seekto=0) or B (if seekto=1). */
int	player_abloop(double a, double b, int seekto);

/* player_cmd executes the given mpv command. The last argument must be NULL. */
int	player_cmd(const char *cmd, ...);

void	player_create(void);
const char*	player_keysym(SDL_Keycode);
mpv_event*	player_next_event(void);
int	player_noabloop(void);

/* player_opt sets an mpv startup option. It terminates the program on error and should
 * only be called to set mpv properties before calling mpv_initialize. */
void	player_opt(const char *opt, const char *fmt, ...);

int	player_paused(void);
int	player_seek(double);

/* player_setprop sets mpv property 'prop' at runtime. Returns non-zero on error. */
int	player_setprop(const char *prop, const char *fmt, ...);
