CC = cc
CCFLAGS = --std=c99 -Werror=implicit -Wuninitialized -Wunused ${CFLAGS}

OBJ = vcut.o player.o

all: vcut

tags: *.c *.h
	ctags -n *.c *.h

vcut: ${OBJ}
	${CC} -o $@ $^ -lmpv $$(sdl2-config --libs)

vcut.o: vcut.c *.h
	${CC} ${CCFLAGS} $$(sdl2-config --cflags) -c $<

%.o: %.c *.h
	${CC} ${CCFLAGS} -c $<

clean:
	rm -f vcut *.o
